"""
作者：Lezs
文件名称：task2_3
时间：2021/3/27 15:00
备注：输入3个数a，n，q(q≠1)，求Sn.
"""
a = int(input("请输入首项a:"))
n = int(input("请输入项数n:"))
q = int(input("请输入公比q:"))
while q == 1:
    print("q不能为1，请重新输入！")
    q = int(input("请输入公比q:"))
Sn = (a * (q ** n - 1)) / (q - 1)
print(f"Sn={int(Sn)}")
