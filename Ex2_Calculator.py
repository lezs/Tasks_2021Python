"""
作者：Lezs
文件名称：Ex2_Caculator
时间：2021/4/26 19:00
备注：能够进行两个数字间四则运算、三角运算、模运算等的小型计算器
"""

import math


def caculate_1():
    """
    输入两个数a、b，根据n值选择进行四则运算(+、-、*、/)
    """
    print("当前运算模式：四则运算")
    n = int(input("请选择(1.加法 2.减法 3.乘法 4.除法)："))
    a = int(input("请输入被运算数："))
    b = int(input("请输入运算数："))
    if n == 1:
        print(a, "+", b, "=", a + b)
    elif n == 2:
        print(a, "-", b, "=", a - b)
    elif n == 3:
        print(a, "*", b, "=", a * b)
    elif n == 4:
        print(a, "/", b, "=", a / b)


def caculate_2():
    """
    输入角度a，转化为弧度b，根据n值选择进行三角运算(sin、cos、tan、asin、acos、atan)
    """
    print("当前运算模式：三角运算")
    a = int(input("请输入角度："))
    n = int(input("请选择(1.正弦 2.余弦 3.正切 4.反正弦 5.反余弦 6.反正切)："))
    b = a * math.pi / 180
    if n == 1:
        print(f"sin({a}°) = {'%.2f' % math.sin(b)}")
    elif n == 2:
        print(f"cos({a}°) = {'%.2f' % math.cos(b)}")
    elif n == 3:
        print(f"tan({a}°) = {'%.2f' % math.tan(b)}")
    elif n == 4:
        print(f"arcsin({a}°) = {'%.2f' % math.asin(b)}")
    elif n == 5:
        print(f"arccos({a}°) = {'%.2f' % math.acos(b)}")
    elif n == 6:
        print(f"arctan({a}°) = {'%.2f' % math.atan(b)}")


def caculate_3():
    """
    输入被模数a和模数b，计算amodb
    """
    print("当前运算模式：模运算")
    a = int(input("请输入被模数："))
    b = int(input("请输入模数："))
    print(f"{a}(mod{b}) = {a % b}")


def caculate_4():
    """
    输入一个数a，计算其阶乘
    """
    print("当前运算模式：阶乘运算")
    a = int(input("请输入计算数："))
    ans = 1
    for i in range(1, a + 1):
        ans = i * ans
    print(f"{a}! = {ans}")


flag = 1
while flag == 1:
    print("运算模式：1.四则运算 2.三角运算 3.模运算 4.阶乘")
    n = int(input("请选择运算模式："))
    if n == 1:
        caculate_1()
    elif n == 2:
        caculate_2()
    elif n == 3:
        caculate_3()
    elif n == 4:
        caculate_4()
    else:
        print("无此选项，请重新选择！")
    flag = int(input("是否继续？(1.继续 2.退出)："))
    print("\n")
