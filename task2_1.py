"""
作者：Lezs
文件名称：task2_1
时间：2021/3/26 23:00
备注：f字符串相关知识练习
"""
first_name = "lezs"
last_name = "zero"
full_name = f"{first_name} {last_name}"
print(f"Hello, {full_name.title()}!")
