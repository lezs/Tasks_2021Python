"""
作者：Lezs
文件名称：task2_2
时间：2021/3/27 10:00
备注：已知E(x)和解密函数D(x).输入一个x，计算E(x)的值y.然后计算D(y)并判断x是否等于D(y).
"""
x = int(input("请输入x:"))
# 计算E(x)的值y
y = (5 * x + 8) % 26
print(f"y的值是{y}")
# 计算D(y)的值D并判断x是否等于D(y)
D = (21 * (y - 8)) % 26
print(f"Is x equal to D(y): {D == x}")
